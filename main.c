//Project : MSP430I2041
//        : ����� �������� ��� ��
//        : 
//        :
//        :
//Version : 1.0
//Date    : 16-12-2018. (DD-MM-YY)
//Author  : ������������ ������� ��������
//Company : ���"�������-��������"
//Comments: IAR Workbench target descriptor for MSP430 version 6.40.1/WIN

//
//Chip type          : MSP430I2041
//Clock frequency    : 10-14 MHz Internal RC 
//Internal SRAM size : 0
//External SRAM size : 0


#include "io430.h"
#include "LCD.h"
#include "LCD.c"
//#include <intrinsics.h>
//#include "math.h"


#define ADC_resolut 16777214
#define width_window_U 64
#define length_window_U 64
#define width_window_I 64
#define length_window_I 64
#define length_window_second_I 5
#define length_window_second_U 5

void Answer_U_I_T (void);
void floating_window (void);
void Send_Answer_U (void);                           //����� ���������� �� LCD
void Send_Answer_I (void);                           //����� ���� �� LCD
void Send_Answer_T (void);                           //����� ����������� �� LCD

unsigned long U024_H, U124_H, U024_L, U124_L;
unsigned char flag              = 0;
float otvet_U                   = 0;                 //���������� �������� ����������
float otvet_I                   = 0;                 //���������� �������� ����
float V_ref                     = 1.1782;            //������� ���������� ���  
unsigned char flag_MEM          = 0;                 //���� ������ �������� ����� ���
unsigned int i                  = 0;
unsigned int j                  = 0;
float T                         = 0;                 //�������� ����������� ��������� �� �����
unsigned char flag_Temper       = 0;

unsigned long *window_buf_U;                         //��������� �� ������ ������� ����������� ����������
unsigned long *window_buf_I;                         //��������� �� ������ ������� ����������� ����
unsigned long *window_buf_answer_U;                  //��������� �� ������ ������� ����������� ����������
unsigned long *window_buf_answer_I;                  //��������� �� ������ ������� ����������� ����

unsigned long window_answer_U[length_window_second_U];
unsigned long window_answer_I[length_window_second_I];
unsigned long window_U[length_window_U];             //������ ��� ���������� ���� ����������
unsigned long window_I[length_window_I];             //������ ��� ���������� ���� ����

volatile unsigned long UU024        = 0;             //�������� � ��� ��� ������� � ������ �0(����������)
volatile unsigned long UU124        = 0;             //�������� � ��� ��� ������� � ������ �1(���)
volatile unsigned long TOK_buf      = 0;             //������� �������� ����� ����������� ����   
volatile unsigned long VOLT_buf     = 0;             //������� �������� ����� ����������� ����������
volatile unsigned long VOLT_answer  = 0;             //�������� ���� ��������� �� �����
volatile unsigned long TOK_answer   = 0;             //�������� ���������� ��������� �� �����
volatile unsigned int Temper        = 0;             //�������� ����������� ������ � ���



int main( void )
{    
  U024_H = U124_H = U024_L = U124_L = 0;
  WDTCTL = WDTPW + WDTHOLD;                           //��������� ����������� �������
  
  P1DIR |= BIT4;                                      //4 ����� 1 ����� �������� �� ����� 
  P1DIR |= BIT5 | BIT6 | BIT7;                        //������ 5 - 7 ����� 1 ��������� �� ����� ����������
  P2DIR |= BIT0 | BIT1 | BIT2 | BIT3;                 //������ 0 - 3 ����� 2 ��������� �� ����� ����������


  
  CSCTL1 |= DIVS__16;                                 //������������ ������� ��� SMCLK ������� �� 16
  TA0CCTL0 = CCIE;                                    //���������� ���������� �� �������� ������� �������� �� ����������         
  TA0CCR0 = 32500;                                    //�����(12069) ��������, ������� ���� �������� ������� ������ 1��
  TA0CTL = TASSEL_2 | MC_1 | ID_3;                    //������������ �� ���� SMCLK/8(������ ���� 1) , ����� �� ������������
  __enable_interrupt();                               //���������� ���� ����������
  
  SD24CTL |= SD24REFS;                                //���������� ������� ���������� 
  SD24CCTL0 |= SD24GRP;                               //������ � ������� 1
  SD24CCTL1 |= SD24GRP;                               
  SD24CCTL2 |= SD24DF | SD24IE;                       //���������� ����������
  SD24INCTL2 |= SD24INCH_6;                           //����� ������� ����������� 
  
  __delay_cycles(3200);                               //�������� ~200 ��� ����� ���������� ������� ���������� ������������ 1.2V 

  SD24CCTL2 |= SD24SC;                                //��������� ���� ������ ��������������

  P1OUT &= 0;
  P2OUT &= 0;
  
  for(int a = 0; a <= (width_window_U - 1); a++)      //��������� ��������
    window_U[a] = 0;

  for(int a = 0; a <= (width_window_I - 1); a++)
    window_I[a] = 0;

  for(int a = 0; a <= (length_window_second_U - 1); a++)
    window_answer_U[a] = 0;

  for(int a = 0; a <= (length_window_second_I - 1); a++)
    window_answer_I[a] = 0;  

  window_buf_I =&window_I[0];                                   //����������� ���������� �� ������ �������
  window_buf_U =&window_U[0];
  window_buf_answer_I =&window_answer_I[0];
  window_buf_answer_U =&window_answer_U[0];

  init();                                             //������������� �� �������
  
  while(1) 
  {        
    if(flag == 1)
    {
      Answer_U_I_T();
      flag = 0;
    }
    if(flag_Temper == 8)
      Send_Answer_T();
  }
}

//************************************
//******* �/� ������� ������� ********
//************************************
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TA0_ISR(void)
{  
  flag++;
  flag_Temper++;
}

//************************************
//******* �/� ���������� � ��� *******
//************************************
#pragma vector=SD24_VECTOR
__interrupt void SD24_ISR(void)
{

  if(flag_MEM == 0)
  {    
    U024_H = SD24MEM0;              
    U024_H = U024_H << 8;                                    //����� �� 8 ��� ����� �������� ������� 16 ���
    U024_H &= 0x00FF0000;                                    //��������� ������� 16 ��� 
    SD24CCTL0 |= SD24LSBACC;                                 //��������� ���� ������ ������� ��� ��� 0

    U124_H = SD24MEM1;
    U124_H = U124_H << 8;                                    //����� �� 8 ��� ����� �������� ������� 16 ���
    U124_H &= 0x00FF0000;                                    //��������� ������� 16 ���
    SD24CCTL1 |= SD24LSBACC;                                 //��������� ���� ������ ������� ��� ��� 1

    flag_MEM = 1;                                            //��������� �����, ����� � ��������� ���������� ����� ������� ��� � ���  
  }
  else
  {
    U024_L = SD24MEM0;
    SD24CCTL0 &= ~SD24LSBACC;                                //����� ���� ������ ������� ��� ��� � ��������� ���������� ����� ������ ������� ���� 
    
    U124_L = SD24MEM1;                                        
    SD24CCTL1 &= ~SD24LSBACC;                                //����� ���� ������ ������� ��� ��� � ��������� ���������� ����� ������ ������� ���� 

    UU024 = U024_H | U024_L;                                 //���������� ������� � ������� ����� � ��� 0
    UU124 = U124_H | U124_L;                                 //���������� ������� � ������� ����� � ��� 1

    flag_MEM = 0;                                            //���� ����� ������� ��� ���
    

    *window_buf_I = UU124;                                   //������ ������� �������� � ������ ��� ����������� ����  
    *window_buf_U = UU024;                                   //������ �������� ���������� � ������ ��� ����������� ����
    window_buf_I += 1;
    window_buf_U += 1;

    if(window_buf_I == (window_I + width_window_I))
      window_buf_I = window_I;
    if(window_buf_U == (window_U + width_window_U))
      window_buf_U = window_U;    
  } 
  
  Temper = SD24MEM2;
    
}

//************************************
//****** �/� ������� ���������� ******
//************************************
void Answer_U_I_T (void)
{
  floating_window();                                        //���������� �������� �������� ���� � ������� ����������� ����  

  if(VOLT_answer >= 0x800000)                               //������� ��������� ������������ �� ����������
  {
    VOLT_answer = VOLT_answer - 0x800000;                                
    otvet_U = 1000*((V_ref/0x800000)*VOLT_answer);          //������ �������� ����������
    mesto(0,1);
    LCD_write_data(' ');                                    //������� ������
  }
  else
  {
    VOLT_answer = 0x800000 - VOLT_answer;
    otvet_U = 1000*((V_ref/0x800000)*VOLT_answer);
    mesto(0,1);
    LCD_write_data('-');                                     //���� ���������� �������������, �� ������� ��������� ���� ����� 
  }

  if(TOK_answer >= 0x800000)
    TOK_answer = TOK_answer - 0x800000;
  else
    TOK_answer = 0x800000 - TOK_answer;
  
  otvet_I = 1000000*(((V_ref/0x800000)*TOK_answer)/49.9 + 0.00000912);   //������� ������� ���� � ��, ������� ���� �������������� ��������� �� 1000

  Send_Answer_U();
  Send_Answer_I();    
}

//************************************
//******** �/� ���������� ���� *******
//************************************
void floating_window (void)
{
  TOK_buf = 0;                                               //��������� ���������� ���
  VOLT_buf = 0;                                              //��������� ���������� VOLT
  TOK_answer = 0;
  VOLT_answer = 0;

  for(int a = 0; a <= (width_window_I - 1); a++)
    TOK_buf = TOK_buf + window_I[a];
  for(int a = 0; a <= (width_window_U - 1); a++)
    VOLT_buf = VOLT_buf + window_U[a];

  TOK_buf = TOK_buf/width_window_I;                          //������ �������� �������� ����
  VOLT_buf = VOLT_buf/width_window_U;                        //������ �������� �������� ����������

  *window_buf_answer_I = TOK_buf;                            //������ ������� �������� � ������ ��� ����������� ����  
  *window_buf_answer_U = VOLT_buf;                           //������ �������� ���������� � ������ ��� ����������� ����
  window_buf_answer_I += 1;
  window_buf_answer_U += 1;

  if(window_buf_answer_I == (window_answer_I + length_window_second_I))
    window_buf_answer_I = window_answer_I;
  if(window_buf_answer_U == (window_answer_U + length_window_second_U))
    window_buf_answer_U = window_answer_U; 

  for(int a = 0; a <= (length_window_second_I - 1); a++)
    TOK_answer = TOK_answer + window_answer_I[a];

  for(int a = 0; a <= (length_window_second_U - 1); a++)
    VOLT_answer = VOLT_answer + window_answer_U[a];

  TOK_answer = TOK_answer/length_window_second_I;            //������ �������� �������� ����
  VOLT_answer = VOLT_answer/length_window_second_U;          //������ �������� �������� ����������
}

//************************************
//****** �/� ������ ���������� *******
//************************************
void Send_Answer_U (void)
{
  

  LCD_write_data(0x30 + (unsigned char)(otvet_U/1000));
  otvet_U = otvet_U - (unsigned int)(otvet_U/1000)*1000;

  LCD_write_data('.');

  LCD_write_data(0x30 + (unsigned char)(otvet_U/100));
  otvet_U = otvet_U - (unsigned int)(otvet_U/100)*100;

  LCD_write_data(0x30 + (unsigned char)(otvet_U/10));
  otvet_U = otvet_U - (unsigned int)(otvet_U/10)*10;

  LCD_write_data(0x30 + (unsigned char)otvet_U);
}

//************************************
//********* �/� ������ ���� **********
//************************************
void Send_Answer_I (void)
{
  mesto(0,2);

  LCD_write_data(0x30 + (unsigned char)(otvet_I/10000));
  otvet_I = otvet_I - (unsigned int)(otvet_I/10000)*10000;

  LCD_write_data(0x30 + (unsigned char)(otvet_I/1000));  
  otvet_I = otvet_I - (unsigned int)(otvet_I/1000)*1000;
  
  LCD_write_data('.');
  
  LCD_write_data(0x30 + (unsigned char)(otvet_I/100));
  otvet_I = otvet_I - (unsigned int)(otvet_I/100)*100;
  
  LCD_write_data(0x30 + (unsigned char)(otvet_I/10));
  otvet_I = otvet_I - (unsigned int)(otvet_I/10)*10;
  
  LCD_write_data(0x30 + (unsigned char)otvet_I);  
}

//************************************
//****** �/� ������ ����������� ******
//************************************
void Send_Answer_T (void)
{  
  mesto(15,1);

  T = (((float)Temper * 1200)/70711 - 273) * 10;                    //������ ����������� � �������� �������
   
  if(T < 0)
  {
    T =-T;
    LCD_write_data('-');
  }
  else
  {
    LCD_write_data(' ');
  }

  LCD_write_data(0x30 + (unsigned char)(T/100));
  T = T - (unsigned int)(T/100)*100;

  LCD_write_data(0x30 + (unsigned char)(T/10));  
  T = T - (unsigned int)(T/10)*10;
  
  LCD_write_data('.');
  
  LCD_write_data(0x30 + (unsigned char)(T));

  flag_Temper = 0;

}
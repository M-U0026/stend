
//************************************
//***** �/� ������������� ������ *****
//************************************
void init_port( void )
{

  P2DIR |= BIT0 |BIT1 | BIT2 | BIT3;                        //��������� ����� 2 0-3 �� ����� ������
  P1DIR |= BIT5 | BIT6 | BIT7; 
  PORT_DATA=0x00;                                           //��������� �������� ����� F
  PORT_COM=0x00;

}

//************************************
//***** �/� ������ ������� � LCD *****
//************************************
void LCD_write_command(unsigned char data)
{
  unsigned char tmp;
  PORT_COM &= ~RS;                                          //��������� RS
  tmp = data;                                               //���������� ������ � �����������
  tmp = tmp>>4;                                             //����� ����� �� 4  
  PORT_DATA &= 0xF0;                                        //��������� F0-F3
  PORT_DATA |= tmp;                                         //������ ������� �������
  
  PORT_COM |= E; 
  __delay_cycles(14000);
  PORT_COM &= ~E;   
  
  __delay_cycles(14000);
  tmp = data;
  tmp &= 0x0F;
  PORT_DATA &= 0xF0;
  PORT_DATA |= tmp;                                         //�������� ������� ������� 
  
  PORT_COM |= E;
  __delay_cycles(14000);
  PORT_COM &= ~E;
  
  __delay_cycles(14000);
}

//************************************
//******* �/� ������������� LCD ******
//************************************
void init(void)
{
  __delay_cycles(210000);  
  PORT_COM &= ~RW;

  PORT_DATA = 0x03;  
  PORT_COM |= E;
  __delay_cycles((int)(F_CPU*0.001));
  PORT_COM &= ~E;
 
  __delay_cycles(57400);  
    PORT_COM |= E;
  __delay_cycles(14000);
  PORT_COM &= ~E;

  __delay_cycles(1400); 
    PORT_COM |= E;
  __delay_cycles(14000);
  PORT_COM &= ~E;
 
  PORT_DATA = 0x02;
  __delay_cycles(14000);  
  PORT_COM |= E;
  __delay_cycles(14000);
  PORT_COM &= ~E;                                          //��������� LCD �� ������ �� 4 ������ ���� 

  LCD_write_command(0x28);
  LCD_write_command(0x28);
  LCD_write_command(0x28);  
  LCD_write_command(0x0E);                                 //����� ������ ����������� D=1, C=1, B=0
  LCD_write_command(0x01);                                 //������� ������
  LCD_write_command(0x06);                                 //����������� ������ ������� ��� ������ I/D=1 S=0 
  LCD_write_command(0x85);  
}

//************************************
//***** �/� ������ ������. � LCD *****
//************************************
void LCD_write_data(unsigned char data)
{
  unsigned char tmp; 
  PORT_COM |= RS;                                         //��������� ������� RS=1
  tmp = data;         
  tmp = tmp>>4;
  PORT_DATA &= 0xF0;
  PORT_DATA |= tmp;                                             //�������� ������� ������� 
  
  PORT_COM |= E;
  __delay_cycles(14000);
  PORT_COM &= ~E;

  tmp = data;
  tmp &= 0x0F;
  PORT_DATA &= 0xF0;
  PORT_DATA |= tmp;                                             //�������� ������� �������
  
  PORT_COM |= E;
  __delay_cycles(14000);
  PORT_COM &= ~E;

  __delay_cycles(14000);
}

//************************************
//***** �/� ������ ������  � LCD *****
//************************************
void LCD_SendString(char *str)
{
  while(*str)
  {                                                         //�������� ������� �� ������
    LCD_proverka_rus(*str++);
  }
}

//************************************
//*** �/� ��������� ������� � LCD  ***
//************************************
void mesto(unsigned char X, unsigned char Y)
{
    unsigned char tmp;
    tmp = 0x00;
    if(Y==1)
    {
      tmp=tmp+X+0x80;                                       //���������� ������� ����� ������� � LCD (1 ������)
    }
    else
    {
      tmp=tmp+X+0x40+0x80;                                  //���������� ������� ����� ������� � LCD (2 ������)
    }
   LCD_write_command(tmp);
}

//************************************
//***** �/� �������� �� ���. ��. *****
//************************************
void LCD_proverka_rus(unsigned char data)
{
  unsigned char buki;
  unsigned char otp;
  otp=0;
  buki=0;
  buki=data;
  if(buki > 0xBF)                                           //�������� ��� ����� �������
  {
    if(buki > 0xDF)                                         //�������� �� ������� 
    {
      if(buki>>4 == 0x0F)
      {
        buki = buki & 0x0F | 0x30;
        otp = a[buki];
      }
      else
      {
        buki = buki & 0x0F | 0x20;
        otp = a[buki];
      }
    }
    else
    {
      if(buki>>4 == 0x0D)                                   //�������� �� ������ 16 ���� �������� ���������
      {
        buki = buki & 0x0F | 0x10;                          //���� ����� ������ 16 �� ����������� 16 
        otp = a[buki]; 
      }                                                     //���������� ���� ����� �� �������
      else 
      {
        buki = buki & 0x0F;                                 //��������� ����� �� ����� ����� � �������
        otp = a[buki]; 
      }                                                     //���������� ���� ����� �� �������
    }
  }
  else
  {
    if(buki == 0xA8)                                        //�������� �� �
      otp = a[64];
    else
    {
      if(buki == 0xB8)                                      //�������� �� �
        otp = a[65];
      else
        otp = buki;  
    }
  }

  LCD_write_data(otp);                                      //�������� �����
}



